public class TurkeyDuckAdapter implements Duck{
    
    private Turkey turkey;

    public TurkeyDuckAdapter(Turkey turkey){
        this.turkey = turkey;
    }
    @Override
    public void fly(){
        turkey.fly();
    }

    @Override
    public void quack(){
        turkey.quack();
    }
}
