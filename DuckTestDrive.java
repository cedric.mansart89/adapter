public class DuckTestDrive {

    //write the duck interface adapter to use as turkey

    public static void main(String[] args) {
        Duck evilDuck = new EvilDuck();
        Turkey madTurkey = new MadTurkey();
        Duck duckMadTurkey = new TurkeyDuckAdapter(madTurkey);
        testDuck(evilDuck);
        testDuck(madTurkey);
    }

    static void testDuck(Duck duck) {
        duck.fly();
        duck.quack();
    }
}